﻿#include "core.h"
#include "render.h"

#include <iostream>
#include <chrono>
#include <cmath>

#include <SDL2/SDL_mixer.h>

using namespace std;

struct GameData
{
    struct Character
    {
        Rect rect = {600, 300, 50, 75};
        enum { Left, Right } direction = Right;
        bool can_jump = false;
        struct Acceleration
        {
            SpeedType x = 0;
            SpeedType y = 1.5;
        } accel;
        struct Speed
        {
            SpeedType x = 0;
            SpeedType y = 0;
        } speed;
    } character;

    struct Display
    {
        ScreenPointType width = 1280;
        ScreenPointType height = 720;
    } display;

    unique_ptr<SDL_Renderer, sdl_deleter> renderer;
    SpeedType jump_force = 10;
    SpeedType max_speedx = 5;
    SpeedType input_accel = 4;
    SpeedType friction = 2;
    bool right_key_pressed = false;
    bool left_key_pressed = false;
    bool up_key_pressed = false;
};


void quit(int code = 0)
{
    Mix_CloseAudio();
    SDL_Quit();
    exit(code);
}


bool point_in_rect(int x, int y, const Rect &r)
{
    return r.x <= x && x <= r.x + r.w &&
           r.y <= y && y <= r.y + r.h;
}


bool collisionlr(const Rect &a, const Rect &b)
{
    return point_in_rect(a.x, a.y, b) ||
           point_in_rect(a.x + a.w, a.y, b) ||
           point_in_rect(a.x, a.y + a.h, b) ||
           point_in_rect(a.x + a.w, a.y + a.h, b);
}


bool collision(const Rect &a, const Rect &b, SDL_Renderer *renderer)
{
    drawRect(a, renderer);
    drawRect(b, renderer);
    return collisionlr(a, b) || collisionlr(b, a);
}

bool handle_events(const SDL_Event &event, GameData &game)
{
    const auto &type = event.type;
    const auto &keysym = event.key.keysym.sym;

    switch (type)
    {
    case SDL_QUIT: return false;
    case SDL_KEYUP:
        switch (keysym)
        {
        case SDLK_q:
        case SDLK_BACKSPACE:
            return false;
        case SDLK_UP:
        case SDLK_w:
            game.up_key_pressed = false;
            std::cout << "Lift up\n";
            break;
        case SDLK_r:
            game.character.rect.x = game.display.width / 2;
            game.character.rect.y = game.display.height - 200;
            break;
        case SDLK_RIGHT:
        case SDLK_d:
            game.right_key_pressed = false;
            std::cout << "Lift right\n";
            break;
        case SDLK_LEFT:
        case SDLK_a:
            game.left_key_pressed = false;
            std::cout << "Lift left\n";
            break;
        }
        break;
    case SDL_KEYDOWN:
        switch (keysym)
        {
        case SDLK_UP:
        case SDLK_w:
            if (game.up_key_pressed == false)
            {
                game.up_key_pressed = true;
                if (game.character.can_jump)
                {
                    game.character.speed.y -= game.jump_force;
                    game.character.can_jump = false;
                }
            }
            std::cout << "Pressed up\n";
            break;
        case SDLK_RIGHT:
        case SDLK_d:
            game.right_key_pressed = true;
            std::cout << "Pressed right\n";
            break;
        case SDLK_LEFT:
        case SDLK_a:
            game.left_key_pressed = true;
            std::cout << "Pressed left\n";
            break;
        }
    }
    return true;
}


void calculations(GameData &game)
{
    if (game.right_key_pressed && !game.left_key_pressed)
    {
        game.character.accel.x = game.input_accel;
        game.character.direction = GameData::Character::Right;
    }
    if (!game.right_key_pressed && game.left_key_pressed)
    {
        game.character.accel.x = -game.input_accel;
        game.character.direction = GameData::Character::Left;
    }
    if (!game.right_key_pressed && !game.left_key_pressed)
        game.character.accel.x = 0;
    if (game.right_key_pressed && game.left_key_pressed)
    {
        if (game.character.speed.x >= 0 && game.character.direction == GameData::Character::Right)
        {
            game.character.accel.x = -game.input_accel;
            game.character.direction = GameData::Character::Left;
        }
        if (game.character.speed.x <= 0 && game.character.direction == GameData::Character::Right)
        {
            game.character.accel.x = game.input_accel;
            game.character.direction = GameData::Character::Left;
        }
    }
    auto &char_speed = game.character.speed;
    std::cout << char_speed.x << " " << game.character.accel.x << "\n";
    char_speed.x += game.character.accel.x;
    char_speed.y += game.character.accel.y;
    if (char_speed.x > 0)
        char_speed.x -= min(char_speed.x, game.friction);
    if (char_speed.x < 0)
        char_speed.x += min(-char_speed.x, game.friction);
    if (char_speed.x > game.max_speedx)
        char_speed.x = game.max_speedx;
    if (char_speed.x < -game.max_speedx)
        char_speed.x = -game.max_speedx;
    if (char_speed.y > game.jump_force)
        char_speed.y = game.jump_force;
    if (char_speed.y < -game.jump_force)
        char_speed.y = -game.jump_force;
    game.character.rect.x += char_speed.x;
    game.character.rect.y += char_speed.y;
    if (game.character.rect.x > game.display.width)
        game.character.rect.x = 0;
    if (game.character.rect.x < 0)
        game.character.rect.x = game.display.width;
}

void charender(GameData::Character &character, const Images::Character &img, SDL_Renderer *r)
{
    static int timer = 0;
    SDL_Rect charr = {character.rect.x, character.rect.y, character.rect.w, character.rect.h};
    const auto &image = [&]() -> const Image & {
        if (!character.can_jump) {
            timer = 0;
            return img.jump;
        }
        if (character.speed.x == 0) {
            timer = 0;
            return img.idle;
        } else {
            return img.steps[(timer++ / 5) % img.steps.size()];
        }
    }();
    switch (character.direction) {
    case GameData::Character::Right:
        image.render(r, charr);
        break;
    case GameData::Character::Left:
        image.render_flipped(r, charr);
        break;
    }

}

void block(const Rect &plat_rect, GameData &game)
{
    if (collision(game.character.rect, plat_rect, game.renderer.get()))
    {
        Rect prev = game.character.rect;
        prev.x -= game.character.speed.x;
        prev.y -= game.character.speed.y;
        if (prev.y >= plat_rect.y + plat_rect.h)
        {
            game.character.rect.y = plat_rect.y + plat_rect.h;
            game.character.speed.y = 0;
        }
        else if (prev.y + prev.h <= plat_rect.y)
        {
            game.character.can_jump = true;
            game.character.rect.y = plat_rect.y - game.character.rect.h;
            game.character.speed.y = 0;
        }
        else if (plat_rect.x + plat_rect.w <= prev.x)
        {
            game.character.rect.x = plat_rect.x + plat_rect.w;
            game.character.speed.x = 0;
        }
        else if (plat_rect.x >= prev.x + prev.w)
        {
            game.character.rect.x = plat_rect.x - game.character.rect.w;
            game.character.speed.x = 0;
        }
        else
        {
            game.character.rect.y = prev.y;
            game.character.rect.x = prev.x;
        }
    }
}


int main()
{
    cout << "Hello, world!\n";
    constexpr int frame_time = 1000/30;
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0)
    {
        std::cerr << "SDL_Init Error: " << SDL_GetError() << std::endl;
        return 1;
    }
    if (Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1)
    {
        std::cerr << "LOL your audio is FUCKED" << std::endl;
        SDL_Quit();
        return -1;
    }
    if (IMG_Init(IMG_INIT_PNG) == 0)
    {
        std::cerr << "IMG_Init Error: " << IMG_GetError() << std::endl;
        quit(-1);
    }
    unique_ptr<SDL_Window, sdl_deleter> window (SDL_CreateWindow("Game Name 0.2",
                                   SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                   1280, 720, SDL_WINDOW_SHOWN));
    if (!window)
    {
        cerr << "SDL_CreateWindow error: " << SDL_GetError() << endl;
        quit(1);
    }

    auto music = std::unique_ptr<Mix_Music, sdl_deleter>(Mix_LoadMUS("main theme.ogg"));
    if (!music || Mix_PlayMusic(music.get(), 4) == -1)
    {
        cerr << "Failed to load music. Bye" << std::endl;
        quit(-1);
    }


    GameData game;
    game.renderer.reset(SDL_CreateRenderer(window.get(), -1, SDL_RENDERER_ACCELERATED));

    Images img;
    img.load_images(game.renderer.get());

    bool menu_open = true;
    while (menu_open)
    {
        auto frame_start_time = chrono::steady_clock::now();
        SDL_Event e;
        while (SDL_PollEvent(&e))
        {
            if (!handle_events(e, game))
            {
                quit();
            }
        }
        calculations(game);

        SDL_RenderClear(game.renderer.get());
        SDL_Rect backr = {0, 0, game.display.width, game.display.height};
        SDL_RenderCopy(game.renderer.get(), img.background.texture.get(), nullptr, &backr);

        game.character.can_jump = false;

        std::array blocks = {Rect {100, 600, 1000, 100},
                             Rect {10, 300, 300, 50},
                             Rect {700, 450, 300, 50}};
        for (const auto &b : blocks) {
            render_block(game.renderer.get(), b, img.platform);
            block(b, game);
        }
        charender(game.character, img.character, game.renderer.get());

        SDL_RenderPresent(game.renderer.get());

        auto frame_end_time = chrono::steady_clock::now();
        chrono::duration<double, std::milli> ftms = frame_end_time - frame_start_time;

        SDL_Delay(max<uint32_t>(0, frame_time - ftms.count()));
    }

    quit();
}
