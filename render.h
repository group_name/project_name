#ifndef RENDER_H
#define RENDER_H

#include "core.h"
#include <iostream>
#include <vector>
#include <SDL_render.h>

struct Image {
    std::unique_ptr<SDL_Texture, sdl_deleter> texture;
    Size size;
    void load(SDL_Renderer *renderer, const char *path)
    {
        texture.reset(IMG_LoadTexture(renderer, path));
        if (!texture)
            std::cerr << "Failed to load texture:" << IMG_GetError();
        SDL_QueryTexture(texture.get(), nullptr, nullptr, &size.x, &size.y);
    }

    void render_flipped(SDL_Renderer *renderer, const SDL_Rect &rect) const
    {
        SDL_RenderCopyEx(renderer, texture.get(), nullptr, &rect, 0, nullptr, SDL_FLIP_HORIZONTAL);
    }

    void render(SDL_Renderer *renderer, const SDL_Rect &rect) const
    {
        SDL_RenderCopy(renderer, texture.get(), nullptr, &rect);
    }
};

struct Images
{
    struct Character {
        std::vector<Image> steps;
        Image idle;
        Image jump;
    } character;
    Image platform;
    Image background;

    void load_images(SDL_Renderer *renderer)
    {
        for (int i = 0; i < 3; ++i) {
            auto &img = character.steps.emplace_back();
            img.load(renderer, ("char2_step" + std::to_string(i+1) + ".png").c_str());
        }
        character.idle.load(renderer, "char2.png");
        character.jump.load(renderer, "char2_step2.png");
        platform.load(renderer, "platform-2.png");
        background.load(renderer, "bg-level-0.png");
    }
};


void drawRect(const Rect &a, SDL_Renderer *renderer)
{
    SDL_Rect r = {a.x, a.y, a.w, a.h};
    SDL_RenderDrawRect(renderer, &r);
}


void render_block(SDL_Renderer *renderer, const Rect &plat_rect, const Image &img)
{
    for (int off_x = 0; off_x < plat_rect.w / img.size.x; off_x++)
    {
        for (int off_y = 0; off_y < plat_rect.h / img.size.y; off_y++)
        {
            SDL_Rect platr = {plat_rect.x + img.size.x * off_x,
                              plat_rect.y + img.size.y * off_y, img.size.x, img.size.y};
            img.render(renderer, platr);
        }
    }
}

#endif // RENDER_H
