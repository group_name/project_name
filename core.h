#ifndef CORE_H
#define CORE_H

#include <memory>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>

using ScreenPointType = int;
using SpeedType = float;

struct Point {
    ScreenPointType x, y;
};
using Size = Point;

struct Rect
{
    ScreenPointType x;
    ScreenPointType y;
    ScreenPointType w;
    ScreenPointType h;
};

struct sdl_deleter
{
    void operator () (SDL_Window *window)
    {
        SDL_DestroyWindow(window);
    }
    void operator () (SDL_Renderer *renderer)
    {
        SDL_DestroyRenderer(renderer);
    }

    void operator () (SDL_Texture *texture)
    {
        SDL_DestroyTexture(texture);
    }

    void operator () (Mix_Music *music)
    {
        Mix_FreeMusic(music);
    }
};

#endif // CORE_H
